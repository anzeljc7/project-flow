# Project Flow
__*Plan*__ vsebuje vse kar je povezano z planiranjem projekta <br />
__*Program*__ vsebuje vse kar je povezano z programiranjem robota <br />
__*PrePrototype*__ večinoma vsebuje modele za preprototype robota. (Preprototype je namenjen temu, da testiramo strojno opremo) <br />