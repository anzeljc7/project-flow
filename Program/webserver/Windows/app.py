from flask import Flask, current_app
app = Flask(__name__)

@app.route('/')
def index():
    return current_app.send_static_file('index.html')
	
@app.route("/forward")
def forward():
	return "forward"

@app.route("/back")
def back():
	return "back"

@app.route("/left")
def left():
	return "left"

@app.route("/right")
def right():
	return "right"
